//
//  TaskDetailVC.swift
//  TaskList
//
//  Created by Tom Odler on 23.01.17.
//  Copyright © 2017 Tom. All rights reserved.
//

import UIKit
import UserNotifications

class TaskDetailVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UNUserNotificationCenterDelegate {

    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var categoryBtn: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var pickerTlbr: UIToolbar!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var notifySwitch: UISwitch!
    @IBOutlet weak var taskDoneSwitch: UISwitch!
    @IBOutlet weak var categoryView: UIView!
    
    var dateForNotification : NSDate = NSDate()
    var selectedCategory : Category = getAllCategories().firstObject as! Category
    var task : Task?
    var center = UNUserNotificationCenter.current()

    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.backgroundColor = UIColor.lightGray
        // Do any additional setup after loading the view.
        if task != nil{
            nameTf.text = task?.name!
            dateForNotification = (task?.date)!
            selectedCategory = (task?.category)!
            taskDoneSwitch.isOn = (task?.done)!
            
            UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (requests) in
                for request in requests{
                    if request.identifier == self.task?.objectID.uriRepresentation().absoluteString{
                        self.notifySwitch.isOn = true
                    }
                }
            })
        }
        
        datePicker.date = dateForNotification as Date
        let index = getAllCategories().index(of: selectedCategory)
        categoryPicker.selectRow(index, inComponent: 0, animated: false)
        
        setViews()
    }
    
    func setViews(){
        categoryBtn.setTitle(selectedCategory.name, for: .normal)
        categoryView.backgroundColor = selectedCategory.color as? UIColor
        
        dateBtn.setTitle(DateFormatter.localizedString(from: datePicker.date, dateStyle: .short, timeStyle: .short), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return getAllCategories().count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let category = getAllCategories()[row] as! Category
        return category.name
    }

    @IBAction func categoryTapped(_ sender: UIButton) {
        nameTf.resignFirstResponder()
        categoryPicker.isHidden = false
        datePicker.isHidden = true
        pickerTlbr.isHidden = false
        doneBtn.tag = 1
    }
    
    @IBAction func dateTapped(_ sender: Any) {
        nameTf.resignFirstResponder()
        datePicker.isHidden = false
        categoryPicker.isHidden = true
        pickerTlbr.isHidden = false
        doneBtn.tag = 2
    }

    
    @IBAction func doneTapped(_ sender: Any) {
        let btn = sender as! UIBarButtonItem
        switch  btn.tag{
        case 1:
            categoryPicker.isHidden = true
            selectedCategory = getAllCategories()[categoryPicker.selectedRow(inComponent: 0)] as! Category
            break
        case 2:
            dateForNotification = (datePicker.date as NSDate?)!
            datePicker.isHidden = true
            break
        default:
            datePicker.isHidden = true
            categoryPicker.isHidden = true
            
        }
        setViews()
        pickerTlbr.isHidden = true
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if (nameTf.text?.characters.count)! > 0{
            if  let myTask = self.task{
                myTask.name = nameTf.text
                myTask.category = selectedCategory
                myTask.date = dateForNotification
                myTask.done = taskDoneSwitch.isOn
                appD.saveContext()
            } else {
                self.task = createTask()
                self.task?.name = nameTf.text
                self.task?.category = selectedCategory
                self.task?.date = dateForNotification
                self.task?.done = taskDoneSwitch.isOn
                appD.saveContext()
            }
            triggerNotification()
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            let controller = UIAlertController.init(title: "Error", message: "Please type a name", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            controller.addAction(ok)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func didBeginEditing(_ sender: Any) {
        pickerTlbr.isHidden = true
        datePicker.isHidden = true
        categoryPicker.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func triggerNotification(){
        center.removePendingNotificationRequests(withIdentifiers: [task!.objectID.uriRepresentation().absoluteString])
    
        if notifySwitch.isOn{
            center.delegate = self
            
            let calendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
            let components = calendar.components([.month, .day, .hour, .minute], from: dateForNotification as Date)
            
            let content = UNMutableNotificationContent()
            content.title = "Task: \(task!.name!)"
            content.body = "Category: \(task!.category!.name!)"
            content.sound = UNNotificationSound.default()
            
            let trigger = UNCalendarNotificationTrigger.init(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: task!.objectID.uriRepresentation().absoluteString, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request)
        }
    }
    
    @IBAction func notifySwitchTapped(_ sender: UISwitch) {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                sender.isOn = false
                let alert = UIAlertController.init(title: "Error", message: "Please, allow notifications in settings", preferredStyle: .alert)
                let okAction = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
